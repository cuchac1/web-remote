Developer Guide
===============

Prerequisites
-------------

In order to get started developing the web remote, you'll need to install the following on your computer:

- NodeJS
- NPM
- Yarn

Or using docker from repository root you can launch commands like this:

`docker run --rm -it -w /app --network host -v .:/app jitesoft/node-yarn:18-slim yarn install`

Get Started
-----------

Once you have the prerequisites installed, in the root directory, run the following command:

.. code::

  yarn install


To run the web remote, run the following command:

.. code::

  yarn start


To build the web remote manually for deployment:

.. code::

  yarn build --aot


Deployment
----------

For deployment, see the `Deployment`_ page.


.. _Deployment: docs/deployment.rst
