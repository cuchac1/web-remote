import {Component, Inject, OnInit} from '@angular/core';
import { OpenLPService } from '../../openlp.service';
import { PageTitleService } from '../../page-title.service';
import {DisplayMode, State} from '../../responses';

@Component({
    selector: 'openlp-remote',
    templateUrl: `./remote.component.html`,
    styleUrls: [`./remote.component.scss`]
})
export class RemoteComponent {

  state = new State();

  constructor(
    protected pageTitleService: PageTitleService,
    protected openlpService: OpenLPService,
  ) {
    pageTitleService.changePageTitle('Remote');
    this.openlpService.stateChanged$.subscribe(item => this.updateState(item));
  }

  protected readonly DisplayMode = DisplayMode;

  updateState(state: State): void {
    this.state = state;
  }

  setMode(mode: DisplayMode): void {
    event.preventDefault();
  }


  nextItem() {
    this.openlpService.nextItem().subscribe();
  }

  previousItem() {
    this.openlpService.previousItem().subscribe();
  }

  nextSlide() {
    this.openlpService.nextSlide().subscribe( );
  }

  previousSlide() {
    this.openlpService.previousSlide().subscribe();
  }


  blankDisplay() {
    this.openlpService.blankDisplay().subscribe();
  }

  themeDisplay() {
    this.openlpService.themeDisplay().subscribe();
  }

  desktopDisplay() {
    this.openlpService.desktopDisplay().subscribe();
  }

  showDisplay() {
    this.openlpService.showDisplay().subscribe();
  }

}
